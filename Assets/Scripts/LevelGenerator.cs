﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public bool needGenerateLevel = false;
    ///
    // Screen borders
    private float minX = -8.5f;
    private float maxX = 8.35f;
    private float minY = -4.5f;
    private float maxY = 2.5f;
    ///

    public GameObject platformPrefab;
    public GameObject platformFixedPrefab;
    public GameObject platformSquarePrefab;
    public GameObject coinPrefub;
    public GameObject unitFirstPrefub;
    public GameObject unitSecondPrefub;

    public GameObject[] hero;

    private HeroController heroController;
    private List<GameObject> objects = new List<GameObject>();


    public Transform platformCheck;
    public LayerMask whatIsPlatform;


    void Start()
    {
        hero = GameObject.FindGameObjectsWithTag("Player");
        heroController = hero[0].GetComponent<HeroController>();
    }

    void Update()
    {
        if (needGenerateLevel)
        {
            generateLevel();
            needGenerateLevel = false;
        }
    }
    /*
    void generateLevel()
    {
        hero[0].transform.position = new Vector3(0, 5.4f, 0);
        heroController.maxAchieves = 1;

        if (platforms.Count > 0)
        {
            for (int i = 0; i < platforms.Count; i++)
            {
                Destroy(platforms[i]);
            }
            platforms.Clear();
        }

        for (int i = 0; i < 3; i++)
        {
            GameObject pl = Instantiate(platformPrefab, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0), Quaternion.identity);
            platforms.Add(pl);
        }
        for (int i = 0; i < 4; i++)
        {
            GameObject pl = Instantiate(platformPrefabMoved, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0), Quaternion.identity);
            platforms.Add(pl);
        }
        for (int i = 0; i < 1; i++)
        {
            GameObject pl = Instantiate(coinPrefub, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0), Quaternion.identity);
            platforms.Add(pl);
        }
    }*/

    void checkPosition(GameObject obj)
    {
        bool result = false;
        int maxTry = 15;
        int tryCounts = 0;
        while(!result)
        {
            if(tryCounts > maxTry)
            {
                obj.transform.position = new Vector3(-20.0f, -20.0f, 0);
                result = true;
                break;
            }

            Vector2 sizeObj = obj.GetComponent<BoxCollider2D>().size;
            bool platformNear = Physics2D.OverlapBox(obj.transform.position, sizeObj, 0, whatIsPlatform);
            if(platformNear)
            {
                float posX = obj.transform.position.x;
                float posY = obj.transform.position.y;
                float newPosX, newPosY;
                if (posX > 0)
                    newPosX = posX - 0.2f;
                else
                    newPosX = posX + 0.2f;

                if (posY > 0)
                    newPosY = posY - 0.2f;
                else
                    newPosY = posY + 0.2f;

                obj.transform.position = new Vector3(newPosX, newPosY, 0);
                tryCounts++;
            }
            else
            {
                result = true;
            }
        }

    }

    void addGameObject(GameObject obj)
    {
        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);

        GameObject pl = Instantiate(obj, new Vector3(x, y, 0), Quaternion.identity);
        objects.Add(pl);

        //checkPosition(pl);
    }

    void addGameObject(GameObject obj, float x, float y)
    {
        GameObject pl = Instantiate(obj, new Vector3(x, y, 0), Quaternion.identity);
        objects.Add(pl);
    }

    void generateLevel()
    {
        hero[0].transform.position = new Vector3(0, 5.4f, 0);
        Rigidbody2D heroRigidBody = hero[0].GetComponent<Rigidbody2D>();
        heroRigidBody.velocity = new Vector2(0.0f, 0.0f);

        if (objects.Count > 0)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                Destroy(objects[i]);
            }
            objects.Clear();
        }

        int platformCount = Random.Range(3, 5);
        int platformsSquareCount = Random.Range(5, 9);
        int coinCount = Random.Range(3, 5);

        for (int i = 0; i < platformCount; i++)
        {
            addGameObject(platformPrefab);
        }
        for (int i = 0; i < platformsSquareCount; i++)
        {
            addGameObject(platformSquarePrefab);
        }

        addGameObject(platformPrefab, 0.0f, 0.0f);

        float xFixed1 = Random.Range(minX, maxX);
        float yFixed1 = Random.Range(minY, maxY);

        float xFixed2 = Random.Range(minX, maxX);
        float yFixed2 = Random.Range(minY, maxY);

        addGameObject(platformFixedPrefab, xFixed1, yFixed1);
        addGameObject(platformFixedPrefab, xFixed2, yFixed2);

        for (int i = 0; i < coinCount; i++)
        {
            addGameObject(coinPrefub);
        }

        addGameObject(unitFirstPrefub, xFixed1, yFixed1 + 0.7f);
        addGameObject(unitSecondPrefub, xFixed2, yFixed2 + 0.7f);

        heroController.maxAchieves = coinCount;
        heroController.maxUnits = 2;
    }

}
