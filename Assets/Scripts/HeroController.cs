﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    // Screen borders
    private float minX = -9.3f;
    private float maxX = 8.85f;
    private float minY = -5.47f;
    private float maxY = 4.4f;

    //
    public float speed;
    public float jumpForce;
    private float moveInput;

    private Rigidbody2D rigidBody;
    private bool facingRight = true;
    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    private int exstraJumps;
    public int extraJumpsValue;

    public bool allAchievesUnitsFinded;

    public int maxAchieves;
    public int achievesFinded;

    public int maxUnits;
    public int unitSaved;

    public bool fallDawn = false;
    //
    private AudioSource audioSource;

    void Start()
    {
        exstraJumps = extraJumpsValue;
        rigidBody = GetComponent<Rigidbody2D>();
        achievesFinded = 0;
        unitSaved = 0;
        allAchievesUnitsFinded = false;
        audioSource = GetComponent<AudioSource>();
        //transform.position = new Vector3(0, 0, 0);
    }

    private void FixedUpdate()
    {

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        moveInput = Input.GetAxis("Horizontal");
        rigidBody.velocity = new Vector2(moveInput * speed, rigidBody.velocity.y);

        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        }
    }

    void Update()
    {
        if (isGrounded == true)
        {
            exstraJumps = extraJumpsValue;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && exstraJumps > 0)
        {
            rigidBody.velocity = Vector2.up * jumpForce;
            exstraJumps--;
        }

        if (transform.position.y > maxY)
        {
            transform.position = new Vector3(transform.position.x, maxY, 0);
        }
        else if (transform.position.y < minY)
        {
            //transform.position = new Vector3(0, 5.4f, 0);
            fallDawn = true;
        }

        if (transform.position.x > maxX)
        {
            transform.position = new Vector3(minX + 0.3f, transform.position.y, 0);
        }
        else if (transform.position.x < minX)
        {
            transform.position = new Vector3(maxX - 0.3f, transform.position.y, 0);
        }
    }

    bool checkAchieveUnits()
    {
        int units = achievesFinded + unitSaved;
        int max = maxAchieves + maxUnits;
        if (units == max)
        {
            return true;
        }
        return false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "achieve")
        {
            Destroy(col.gameObject);
            achievesFinded++;
            if (checkAchieveUnits())
                allAchievesUnitsFinded = true;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "unit")
        {
            Destroy(coll.gameObject);
            audioSource.Play();
            unitSaved++;
            if (checkAchieveUnits())
                allAchievesUnitsFinded = true;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
}
