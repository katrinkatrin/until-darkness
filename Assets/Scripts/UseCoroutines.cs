﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement;

public enum GameState
{
    Start,
    PlayRound,
    Pause,
    End
};

public class UseCoroutines : MonoBehaviour
{
    // Screen borders
    //private float minX = -9.3f;
    //private float maxX = 8.85f;
    //private float minY = -5.47f;
    //private float maxY = 4.4f;
    //
    public GameState currentState;
    public float PlayRoundTime = 10.0f;
    private float StartRoundTime = 0.0f;

    private bool needGenerateNewLevel = false;
    /*
    public GameObject platformPrefabMoved;
    public GameObject platformPrefab;
    public GameObject coinPrefub;
*/
    public GameObject[] hero;
    private HeroController heroController;

    //private List<GameObject> platforms = new List<GameObject>();
    
    public GameObject alphaManager;
    private AlphaController alphaController;

    public GameObject generator;
    private LevelGenerator levelGenerator;

    //public int maxCountLevels = 2;
    //private int levelCounter = 0;

    void Start()
    {
        currentState = GameState.Start;
        needGenerateNewLevel = true;

        levelGenerator = generator.GetComponent<LevelGenerator>();
        alphaController = alphaManager.GetComponent<AlphaController>();
        hero = GameObject.FindGameObjectsWithTag("Player");
        heroController = hero[0].GetComponent<HeroController>();
    }

    // Random Generation! //
    /* void generateLevel()
    {
        hero[0].transform.position = new Vector3(0, 5.4f, 0);
        heroController.maxAchieves = 1;

        if (platforms.Count > 0)
        {
            for (int i = 0; i < platforms.Count; i++)
            {
                Destroy(platforms[i]);
            }
            platforms.Clear();
        }

        for (int i = 0; i < 3; i++)
        {
            GameObject pl = Instantiate(platformPrefab, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0), Quaternion.identity);
            platforms.Add(pl);
        }
        for (int i = 0; i < 4; i++)
        {
            GameObject pl = Instantiate(platformPrefabMoved, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0), Quaternion.identity);
            platforms.Add(pl);
        }
        for (int i = 0; i < 1; i++)
        {
            GameObject pl = Instantiate(coinPrefub, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0), Quaternion.identity);
            platforms.Add(pl);
        }
    } */

    void Update()
    {
        if (needGenerateNewLevel && alphaController.stateFadeDone)
        {
            needGenerateNewLevel = false;
            // alphaController.needGenerateLevel = false;
            //generateLevel();
            levelGenerator.needGenerateLevel = true;
        }
      

        if (alphaController.stateClarDone && currentState == GameState.Start)
        {
            currentState = GameState.PlayRound;
            //needGenerateNewLevel = false;
        }
        else if (StartRoundTime < PlayRoundTime &&
            currentState == GameState.PlayRound)
        {
            StartRoundTime = StartRoundTime + Time.deltaTime;
            if (heroController.allAchievesUnitsFinded)
            {
                alphaController.needFade = true;
                alphaController.needClar = true;
                needGenerateNewLevel = true;

                heroController.allAchievesUnitsFinded= false;
                heroController.achievesFinded = 0;
                heroController.unitSaved = 0;
                currentState = GameState.Start;
                StartRoundTime = 0.0f;
            }
        }
        else if (StartRoundTime >= PlayRoundTime &&
            currentState == GameState.PlayRound)
        {
            alphaController.needFade = true;
            alphaController.needClar = true;
            needGenerateNewLevel = true;

            heroController.allAchievesUnitsFinded = false;
            heroController.achievesFinded = 0;
            heroController.unitSaved = 0;
            currentState = GameState.Start;
            StartRoundTime = 0.0f;
        }
        else if(heroController.fallDawn &&
            currentState == GameState.PlayRound)
        {
            heroController.fallDawn = false;
            alphaController.needFade = true;
            alphaController.needClar = true;
            needGenerateNewLevel = true;

            heroController.allAchievesUnitsFinded = false;
            heroController.achievesFinded = 0;
            heroController.unitSaved = 0;
            currentState = GameState.Start;
            StartRoundTime = 0.0f;
        }
    }

}
