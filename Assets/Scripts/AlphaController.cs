﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.SceneManagement;

public class AlphaController : MonoBehaviour
{
    public GameObject AlphaObj;

    public float fadeTimeCoeff = 1.0f;
    public float clarTimeCoeff = 1.0f;

    public bool needFade = false;
    public bool needClar = false;
    public bool stateClarDone = false;
    public bool stateFadeDone = false;

    //public bool needChangeScene = false;

    //public GameObject TextObj;
    //private Image TextImage;
    //private int TextAlphaA;

    private Image AlphaImage;
    private int AlphaA;

    public GameObject Background;
    public SpriteRenderer BackgroundImage;

    private byte r = 0;
    private byte g = 0;
    private byte b = 0;
    private byte a = 0;

    //private bool intro = true;

    private Color32 originalColor;

    void Start()
    {
        stateFadeDone = true;        
        StartCoroutine(Clarification());
    }
    /*
    IEnumerator Intro()
    {
        while (intro)
        {

            TextImage = TextObj.GetComponent<Image>();
            TextImage.color = new Color(TextImage.color.r,
                TextImage.color.g, TextImage.color.b,
                TextImage.color.a + clarTimeCoeff * Time.deltaTime);

            if (TextImage.color.a >= 1)
            {
                intro = false;
                StartCoroutine(Clarification());
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    */

    IEnumerator Clarification()
    {
        while (!stateClarDone)
        {
            AlphaImage = AlphaObj.GetComponent<Image>();
            AlphaImage.color = new Color(AlphaImage.color.r,
                AlphaImage.color.g, AlphaImage.color.b,
                AlphaImage.color.a - fadeTimeCoeff * Time.deltaTime);

            //if(needChangeScene && AlphaImage.color.a < 0.95f)
            //{
            //    needChangeScene = false;
            //    SceneManager.LoadScene("end");
            //}

            if (AlphaImage.color.a <= 0)
            {
                stateClarDone = true;
                stateFadeDone = false;
                //needGenerateLevel = false;
                //GameStateC.currentState = GameState.PlayRound;
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator Fade()
    {
        while (stateClarDone)
        {
            AlphaImage = AlphaObj.GetComponent<Image>();
            AlphaImage.color = new Color(AlphaImage.color.r,
                AlphaImage.color.g, AlphaImage.color.b,
                AlphaImage.color.a + clarTimeCoeff * Time.deltaTime);

            if (AlphaImage.color.a >= 1)
            {
                stateClarDone = false;
                stateFadeDone = true;
                //needGenerateLevel = true;
                if (needClar)
                {
                    needClar = false;
                    StartCoroutine(Clarification());
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKey(KeyCode.E))
        {
            // multiple effect
            BackgroundImage = Background.GetComponent<SpriteRenderer>();
            originalColor = Background.GetComponent<SpriteRenderer>().color;

           // byte temp = (byte)(10 * Time.deltaTime);
            r = originalColor.r;    // + byte(temp);
            g = originalColor.g;
            b = originalColor.b;
            a = originalColor.a;

            //BackgroundImage.color = new Color32(r, g, b, a);

            //r = (AlphaImage.color.r) * (BackgroundImage.color.r);
            //g = (AlphaImage.color.g) * (BackgroundImage.color.g);
            //b = (AlphaImage.color.b) * (BackgroundImage.color.b);

            BackgroundImage.color = new Color(r, g, b, BackgroundImage.color.a - 0.5f * Time.deltaTime);
            //BackgroundImage.color = new Color(BackgroundImage.color.r,
              //     BackgroundImage.color.g, BackgroundImage.color.b,
                //   BackgroundImage.color.a - 0.5f * Time.deltaTime);
        }
        */

        //if (GameStateC.currentState == GameState.Pause)
        //{

        //}

        if(needFade)
        {
            needFade = false;
            StartCoroutine(Fade());
        }
       
    }
}
