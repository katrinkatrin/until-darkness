﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaIntroManager : MonoBehaviour
{
    public GameObject TextObj;
    private Image TextImage;

    private float stepAlpha = 0.05f;
    private bool need_final = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        while (need_final)
        {
            TextImage = TextObj.GetComponent<Image>();
            TextImage.color = new Color(TextImage.color.r,
                TextImage.color.g, TextImage.color.b,
                TextImage.color.a - 0.5f * Time.deltaTime);

            if (TextImage.color.a <= 0)
            {
                need_final = false;
            }
        }
    }
}
