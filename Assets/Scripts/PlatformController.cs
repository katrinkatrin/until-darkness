﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformController : MonoBehaviour
{
    [SerializeField]
    //[HideInInspector]
    private int speedy = 8;
    public int speedx = 5;


    private bool canMove = false;

    //public GameObject hero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //hero = GameObject.Find("hero").transform.position;
        //private heroPosition = hero.transform.position;
        //if (heroPosition.x ==
        if (canMove)
        { 
            float verticalInput = Input.GetAxis("Vertical");
            //transform.Translate(Vector3.up * Time.deltaTime * speedy);
            transform.Translate(Vector3.right * Time.deltaTime * speedx);
        }
    }
    /*
    void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.tag == "Player")
        {
            Renderer render = GetComponent<Renderer>();
            render.material.color = Color.green;
            canMove = true;
        }
    }*/

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            canMove = true;
        }
    }

}
